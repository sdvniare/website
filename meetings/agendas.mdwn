[[!meta title="Meeting agendas"]]
[[!meta copyright="Copyright © 2010-2022 Software in the Public Interest, Inc."]]
[[!meta license="Creative Commons Attribution-ShareAlike 3.0 Unported"]]

# Meeting agendas

Next meeting:

* [Monday, 12th December, 2022](2022/2022-12-12/)


Recent meetings:

* [Monday, 14th November, 2022](2022/2022-11-14/)
* [Monday, 10th October, 2022](2022/2022-10-10/)
* [Monday, 12th September, 2022](2022/2022-09-12/)
* [Monday, 8th August, 2022](2022/2022-08-08/)
* [Monday, 11th July, 2022](2022/2022-07-11/)
* [Monday, 13th June, 2022](2022/2022-06-13/)
* [Monday, 9th May, 2022](2022/2022-05-09/)
* [Monday, 11th April, 2022](2022/2022-04-11/)
* [Monday, 28th March, 2022](2022/2022-03-28/)
* [Monday, 14th March, 2022](2022/2022-03-14/)
* There was no meeting scheduled for January 2022

Agendas by year:

[[!inline pages="meetings/agendas/* and ! meetings/agendas/*/*" sort="-title" template=unordered-list-title archive=yes]]
